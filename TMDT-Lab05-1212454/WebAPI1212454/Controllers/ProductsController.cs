﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212454.Models;

namespace WebAPI1212454.Controllers
{
    //[RoutePrefix("api/2.0")]
    public class ProductsController : ApiController
    {
        List<Product> _products = new List<Product> {
            new Product { Id = 1, Name = "Tomato Soup", Category = "Groceries", Price = 1, Username = "1212454" },
            new Product { Id = 2, Name = "Yo-yo", Category = "Toys", Price = 3.75M, Username = "Kaes" },
            new Product { Id = 3, Name = "Hammer", Category = "Hardware", Price = 16.99M, Username = "Gend" },
            new Product { Id = 4, Name = "Lego", Category = "Toys", Price = 5, Username ="Kaes" }
        };

        [Route ("api/2.0/products")]
        public IEnumerable<Product> GetProducts (string category)
        {
            return _products.Where(p => p.Category == category);
        }

        [Route("api/2.0/products/{category}/{username}")]
        public IEnumerable<Product> GetProducts (string category, string username)
        {
            return _products.Where(p => p.Category == category && p.Username == username);
        }
    }
}
