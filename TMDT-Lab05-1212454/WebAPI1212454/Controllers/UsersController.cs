﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212454.Models;

namespace WebAPI1212454.Controllers
{
    [RoutePrefix("api/2.0/users")]
    public class UsersController : ApiController
    {
        List<User> _users = new List<User> {
            new User { Username = "1212454", Password = "1234"},
            new User { Username = "Kaes", Password = "1234"},
            new User { Username = "Gend", Password = "1234"}
        };

        [Route("{username}")]
        [HttpGet]
        public User Get(string username)
        {
            return _users.FirstOrDefault(u => u.Username == username);
        }

        [Route("")]
        public IEnumerable<User> GetAll()
        {
            return _users;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Add(User user)
        {
            if(_users.Exists(u => u.Username == user.Username))
            {
                return BadRequest("Adding user existed");
            }
            _users.Add(user);
            return Ok("Add Success!");
        }

        [Route("{username}")]
        [HttpDelete]
        public IHttpActionResult Delete(string username)
        {
            var deleteUser = _users.FirstOrDefault(u => u.Username == username);
            if(_users.Remove(deleteUser))
            {
                return Ok("Delete success!");
            }
            return BadRequest("User does not exist");
        }

        [Route("{username}")]
        [HttpPut]
        public IHttpActionResult Update(User user)  
        {
            var updateUser = _users.FirstOrDefault(u => u.Username == user.Username);
            if(updateUser != null)
            {
                _users.Remove(updateUser);
                _users.Add(user);
                return Ok("Update success!");
            }
            return BadRequest("User does not exist");
        }
    }
}
